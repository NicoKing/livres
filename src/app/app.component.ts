import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){
    const config = {
      apiKey: "AIzaSyA0jgq_a8Kk-rRuH2iGOGQ1Tdabgxq7OLI",
      authDomain: "projetopen2.firebaseapp.com",
      databaseURL: "https://projetopen2.firebaseio.com",
      projectId: "projetopen2",
      storageBucket: "projetopen2.appspot.com",
      messagingSenderId: "149128620856"
    };
    firebase.initializeApp(config);
  }
}
